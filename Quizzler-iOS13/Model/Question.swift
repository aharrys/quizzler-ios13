//
//  Question.swift
//  Quizzler-iOS13
//
//  Created by Akhmad Harry Susanto on 19/04/20.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import Foundation

struct Question {
    let text : String
    let answer : String
    
    
    init(q: String, a: String) {
        text = q
        answer = a
    }
}
